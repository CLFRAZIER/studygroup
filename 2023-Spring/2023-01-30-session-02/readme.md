# Member presentation

## Session proposed by Cristian

**Subject**: regular expressions, and its use in Python with a search example

**Background material**: 
- Chapter 6 of book [Python for the Life Sciences – A Gentle Introduction](https://bcrf.biochem.wisc.edu/2022/11/15/python-for-the-life-sciences-a-gentle-introduction/)
- [Chapter 6 code](https://github.com/amberbiology/py4lifesci/blob/master/PFTLS_Chapter_06.py)

**Short info** on regular expressions: file `regex_intro.md`

**Downloads**: 
- Python code files: `reg_expressions.py` and/or `reg_expressions.ipynb`   
- sequence file: `chrY.fna.gz`


